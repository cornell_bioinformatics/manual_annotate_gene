#!/usr/bin/env python3
import sys
infile = sys.argv[1]
outfile = sys.argv[2]
from Bio import SeqIO
wh = open(outfile, "w")
with open(infile, "rU") as input_handle:
    for record in SeqIO.parse(input_handle, "fasta"):
        SeqIO.write(record, wh, "fasta")
