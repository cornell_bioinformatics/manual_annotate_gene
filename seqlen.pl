#!/usr/bin/env perl
($file) = (@ARGV);


use Bio::SeqIO;

if ($file=~/gz$/)
{
 $in  = Bio::SeqIO->new(-file => "gunzip -c $file|" , '-format' => 'Fasta');
}
else
{
	$in  = Bio::SeqIO->new(-file => "$file" , '-format' => 'Fasta');
}
#open OUT, ">${file}.len";
LOOP:while ($seqobj = $in->next_seq())
{
		$id = $seqobj->display_id(); 

		$len = $seqobj->length();
		print  $id, "\t", $len, "\t", $seqobj->desc, "\n";
}
