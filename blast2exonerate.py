#!/usr/bin/env python3
import sys
import math
## blast command
## tblastn -num_threads 60 -query cacna1_uniq.faa -db genome.fasta -outfmt 6 -evalue 1e-50 -out blastout &

## blast input blast outfmt 6, query protein, target genome
blastin = sys.argv[1]
evalucutoff = float(sys.argv[2])

## genome fasta file
#genomein = sys.argv[2]
## evalue cutoff
#evalucutoff = 1e-10


wh= open(f"{blastin}.bed", "wt") 

idcount =0
with open(blastin, "rt") as rh:
    for line in rh:
        line=line.strip().split("\t")
        query = line[0]
        chr = line[1]
        start = int(line[8])-1
        end = int(line[9])
        evalue = float(line[10])
        score = round(- math.log(evalue, 10))

        if (evalue>evalucutoff):
            continue

        id = f"{query}_{idcount}"
        idcount += 1
        strand = "+"

        if (start>end):
            t=start
            start = end
            end = t
            strand ="-"
        wh.write(f"{chr}\t{start}\t{end}\t{id}\t{score}\t{strand}\n")
    rh.close()
wh.close()

# blast protein agains genome to get hsps
#makeblastdb -in wi.fasta -dbtype nucl
#make bed file
#cut -f
#tblastn -num_threads 50 -out wi_blast -db wi.fasta -query cac.faa -evalue 1e-10 -outfmt 6
# convert blastresults to bed format
# run this script
# ~/tools/seqlen.pl genome.fasta > genome.txt
#sort -k1,1V -k2,2n wi_blast.bed | bedtools merge -d 500000  | bedtools slop -i stdin -g genome.txt -b 500000 > new.bed
#bedtools getfasta  -fi genome.fasta -bed new.bed > new.fasta

# exonerate -q cacna1_uniq.faa -Q protein -t new2.fasta -T dna -m protein2genome -n 5 --minintron 20 --maxintron 50000  -f -100 --showtargetgff yes --showvulgar no --showalignment no  > exonerate.gff

# python ~/tools/exonerate2gff3.py exonerate_c.gff

#gffread -g new2.fasta -w t exonerate_b.gff.gff3

