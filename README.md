### Manually annotate a protein in a genome assembly

*Required input files:*

1. protein fasta file: queryProtein.fasta
2. genome fasta file: genome.fasta
3. genome.txt file: two column tab-delimited (chr, length)
(Use this script to create if you do not have the file: seqlen.pl genome.fasta > genome.txt)

*Software dependencies:*

1. blast+
2. exonerate
3. bedtools
4. gffread (from cufflinks)

##### Step 1. blast protein agains genome to get hsps

```
makeblastdb -in genome.fasta -dbtype nucl
tblastn -num_threads 50 -out blastOutput -db genome.fasta -query queryProtein.fasta -evalue 1e-10 -outfmt 6
```



##### Step 2. convert blastresults to bed format (parameter: blast output, evalue cutoff)

```
blast2exonerate.py blastOutput 1e-10
```

output is blastOutput.txt.bed

##### Step 3. merged HSP in previous bed file into a block, extend 5' and 3' end of the block

```
sort -k1,1V -k2,2n blastOutput.bed | bedtools merge -d 500000  | bedtools slop -i stdin -g genome.txt -b 200000 > new.bed
```

* modify the gap (-d, maximum intron)  and extension (-b, extension to both ends of mapped genomic region to run exonerate ) size if needed

##### Step 4. create a fasta file of the candidate genomic region

```
bedtools getfasta  -fi genome.fasta -bed new.bed > new.fasta

#the fasta file created by getfasta does not work with exonerate, this script would add line breaks to the new.fasta file. 
cleanseq.py new.fasta new2.fasta
```

##### Step 5. run exonerate 

Manual: https://www.ebi.ac.uk/about/vertebrate-genomics/software/exonerate-manual)

```
exonerate -q queryProtein.fasta -Q protein -t new2.fasta -T dna -m protein2genome -n 5 --minintron 20 --maxintron 50000  -f -100 --showtargetgff yes --showvulgar no --showalignment no  > exonerate.gff
```

need to adjust (default -f -29): --maxintron 50000  -f -100



##### Step 6. convert exonerate.gff to gff3, output exonerate.gff.gff3

```
exonerate2gff3.py exonerate.gff
```

##### Step 7. generate a CDS fasta file

```
gffread -g new2.fasta -w t exonerate_b.gff.gff3
```

##### Step 8. inspect the results
You might need to expand the maxintron setting in exonerate, or manually merge block

##### New code:
exonerate.py: once finalize the parameter, run this code on a batch of proteins. 
