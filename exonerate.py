#!/usr/bin/env python3
from __future__ import print_function
import os
import sys
from shutil import rmtree
import argparse
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO
import multiprocessing
import math
import re

#some constants
min_python = (3,5) # python version check mininum 3.5



#check python version
try:
    assert(sys.version_info >= min_python)
except:
    from platform import python_version
    print("Python version", python_version(), "is too old.")
    print("Please use Python", ".".join(map(str,min_python)), "or later.")
    sys.exit()




def main():
    global args
    global tmpDir 
    global evaluecutoff
    global blastCmd
    global bedtoolsCmd
    global exonerateCmd
    global GFFReadCmd

    blastCmd = "tblastn"
    bedtoolsCmd = "bedtools"
    exonerateCmd = "exonerate"
    GFFReadCmd = "gffread"

    parser = argparse.ArgumentParser(description='Description of my function')
    #parser.add_argument('-h','--help',type=str,required=False,default="",help='help text')
    parser.add_argument('-d','--db',type=str,required=True,default="",help='Genome assembly database in blast format. Must included a genome fasta file with the same name!!!')
    parser.add_argument('-g','--chrSizeFile',type=str,required=True,default="",help='A text file of chromosome size. It can be created with "samtools faidx genome.fasta; cut -f1,2 genome.fasta.fai> genome.txt"')
    parser.add_argument('-q','--query',type=str,required=True,default="",help='Query fasta file.')
    parser.add_argument('-o','--out',type=str,required=True,default="",help='Output directory.')
    parser.add_argument('-j','--jobs',type=str,required=False,default="8", help='Number of jobs. (default: 4)')
    parser.add_argument('-t','--threads',type=str,required=False,default="4", help='Number of threads. (default: 2)')
    parser.add_argument('-e','--evalue',type=str,required=False,default="1e-20",help='blast evalue cutoff. (default: 1e-20)')
    parser.add_argument('-i','--intron',type=str,required=False,default="1200000",help='max intron size. (default: 1,200,000)')
    parser.add_argument('-u','--extension',type=str,required=False,default="1200000",help='extension to 5prime and 3prime fragment before exonerate. (default: 1,200,000)')
    parser.add_argument('-m','--minLen',type=str,required=False,default="250",help='minimum input sequence length. (default: 250)')
    parser.add_argument('-f','--frameshift',type=str,required=False,default="-28",help='exonerate frameshift penalty. (default: -28)')
    parser.add_argument('-p','--percentage',type=str,required=False,default="0.5",help='minimum coverage percentage. (default: 0.5)')


    args=parser.parse_args()


    evaluecutoff = float(args.evalue)

    if os.path.exists(args.out):
        print(f"Error: the output directory {args.out} exits. Cannot overwrite!")
        sys.exit()

    os.makedirs(args.out)
    tmpDir = args.out+ "/tmp"
    os.makedirs(tmpDir);

    jobList = []
    jobId = 0
    skippedP = 0
    with open(args.query, "rt") as handle:
        for record in SeqIO.parse(handle, "fasta") :
            if (len(record.seq)>int(args.minLen)):
                jobId += 1
                jobList.append((jobId, record))
            else:
                skippedP +=1
    handle.close()

    print (f"Total proteins to process: {jobId}" )
    print (f"Skipped protein due to size: {skippedP}")
    pool = multiprocessing.Pool(processes= int(args.jobs))
    pool.starmap(process_gene, jobList)
    pool.close()

#    proteinFasta =  open(f'{args.out}/protein.fasta', 'w') 
#    CDSFasta =  open(f'{args.out}/CDS.fasta', 'w')
#    INFO =  open(f'{args.out}/info.txt', 'w') 
#    INFO.write("queryName\tqueryLen\tqueryDesc\tScaffold\tStart\tEnd\tGenomicLen\tRNALen\n")
#    for result in pool.imap(process_gene, jobList):
#        if (result):
#            infoStr, proteinStr, cdsStr = result.split("$$!$")
#            INFO.write(infoStr)
#            proteinFasta.write(proteinStr)
#            CDSFasta.write(cdsStr)
#
#    INFO.close()
#    CDSFasta.close()
#    proteinFasta.close()
#    pool.close()     


def process_gene(jobId, querySeq):
    jobDir =  f"{tmpDir}/{jobId}"
    os.mkdir(jobDir)
    queryFile = f"{jobDir}/query.fas"
    blastOutFile = f"{jobDir}/blastout"
    blastBEDFile = f"{jobDir}/blastoutbed"
    mergedBED = f"{jobDir}/mergedBED"
    localGenomeFASTA = f"{jobDir}/localFASTA"
    localGenomeFASTACleaned = f"{jobDir}/localFASTACleaned"
    exonerateGFF = f"{jobDir}/exonerateGFF"
    outGFF = f"{jobDir}/out.gff3"
    outProtein = f"{jobDir}/out.protein"
    outRNA = f"{jobDir}/out.RNA"
    outInfo = f"{jobDir}/out.info"
    
    queryLen = len(querySeq.seq)
    queryName = querySeq.id
    queryDesc = querySeq.description
    with open (queryFile, "w") as wh1:
        SeqIO.write(querySeq, wh1, "fasta")
        wh1.close()

    ## blast query against genome
    myBlastCmd = f"{blastCmd} -num_threads {args.threads} -out {blastOutFile} -db {args.db} -query {queryFile} -evalue {args.evalue} -outfmt 6"
    results = os.system(myBlastCmd)
    if (results>0):
        print("Error: BLAST error\n")

    if (os.path.getsize(blastOutFile) ==0):
        rmtree(jobDir)
        return ("")

    ## convert blast output to a bed file
    wh= open(blastBEDFile, "wt") 
    idcount =0
    with open(blastOutFile, "rt") as rh:
        for line in rh:
            line=line.strip().split("\t")
            query = line[0]
            chr = line[1]
            start = int(line[8])-1
            end = int(line[9])
            evalue = float(line[10])
            if evalue==0:
                evalue=1e-200
            score = round(- math.log(evalue, 10))

            if (evalue> evaluecutoff):
                continue

            id = f"{query}_{idcount}"
            idcount += 1
            strand = "+"

            if (start>end):
                t=start
                start = end
                end = t
                strand ="-"
            wh.write(f"{chr}\t{start}\t{end}\t{id}\t{score}\t{strand}\n")
        rh.close()
    wh.close()

    ## sort the bed file
    mySortCmd = f"sort -k1,1V -k2,2n {blastBEDFile} | {bedtoolsCmd} merge -d {args.intron}  | {bedtoolsCmd} slop -i stdin -g {args.chrSizeFile} -b {args.extension} > {mergedBED}"
    results = os.system(mySortCmd)
    if (results>0):
        print("Error: Sort/merge BED failed\n")


    ## create local fasta file
    myCreateFastaCmd = f"{bedtoolsCmd} getfasta  -fi {args.db} -bed {mergedBED} > {localGenomeFASTA}"
    results = os.system(myCreateFastaCmd)
    if (results>0):
        print("Error: create local genome fasta file failed\n")

    ## clean the local genome sequence file
    wh = open(localGenomeFASTACleaned, "w")
    with open(localGenomeFASTA, "rU") as input_handle:
        for record in SeqIO.parse(input_handle, "fasta"):
            SeqIO.write(record, wh, "fasta")
        input_handle.close()
    wh.close()

    ## run exonerate
    myExonerateCmd = f"{exonerateCmd} -q {queryFile} -Q protein -t {localGenomeFASTACleaned} -T dna -m protein2genome --bestn 3 --minintron 20 --maxintron {args.intron}  -f {args.frameshift} --showtargetgff yes --showvulgar no --showalignment no  > {exonerateGFF}"

    results = os.system(myExonerateCmd)
    if (results>0):
        print("Error: exonerate failed\n")

    ## exonerate result to gff3
    gid = "xxxx"
    gid2RNAlen = {}
    gid2exons = {}
    gid2gfflines = {}
    
    geneIndex =0
    with open (exonerateGFF, "r") as rh:
        for line in rh:

            line=line.strip().split("\t")
            if ((len(line)>5) and (line[2]=="gene")):
                scaffold=line[0]
                start = line[3]
                end = line[4]
                annot=line[8]
                match = re.search("sequence (\S+)", annot)
                query = match[1]
                geneIndex += 1
                gid = f"{geneIndex}__{scaffold}.{start}.{end}"
                gid2RNAlen[gid] =0 
                gid2exons[gid]=0
                gid2gfflines[gid]= ""
                line[8] = f"ID={gid};Name={gid}" 
                gid2gfflines[gid] +="\t".join(line) + "\n"

                line[2]="mRNA"
                line[8] = f"ID={gid}-r;Parent={gid}" 
                gid2gfflines[gid] +="\t".join(line) + "\n"


            if ((len(line)>5) and (line[2]=="cds")):
                CDSlen = int(line[4]) - int(line[3])
                gid2RNAlen[gid] += CDSlen

                line.append("")
                line[2] = "exon"
                line[8] = f"ID={gid}-e;Parent={gid}-r"
                gid2gfflines[gid] +="\t".join(line) + "\n"

                line[8] = f"ID={gid}-cds;Parent={gid}-r"
                line[2] = "CDS"
                gid2gfflines[gid] +="\t".join(line) + "\n"
                gid2exons[gid] += 1
        rh.close()
    #get the longest CDS 
    if (len(gid2RNAlen)>0):
        largestProtein = sorted(gid2RNAlen.items(), key=lambda x: x[1], reverse=True)[0]
        
        largestGeneName = largestProtein[0]
        largestGeneRNALen = largestProtein[1]

        percentage = float(args.percentage)
        if (largestGeneRNALen/(queryLen*3) < percentage):
            rmtree(jobDir)
            return ("")


        matchName = re.search("__(\S+):(\d+)-(\d+)\.(\d+)\.(\d+)$", largestGeneName)
        largestGeneGenomicLen = 0
        largestGeneScaffold = "N/A"
        largestGeneStart = 0
        largestGeneEnd = 0

        if matchName:
            largestGeneScaffold = matchName[1]
            regionStart = int(matchName[2])
            regionEnd = int(matchName[3])
            geneStart = int(matchName[4])
            geneEnd = int(matchName[5])
            largestGeneGenomicLen= geneEnd - geneStart + 1
            largestGeneStart = regionStart + geneStart 
            largestGeneEnd = largestGeneStart +  largestGeneGenomicLen -1


        
        
        largestGeneExons = gid2exons[largestGeneName]


        ## get genomic DNA sequence

        wh = open (outGFF, "w")
        wh.write(gid2gfflines[largestGeneName])
        wh.close()

        ## get protein and mRNA sequence
        myGFFReadCmd = f"{GFFReadCmd} -g {localGenomeFASTACleaned} -w {outRNA} -y {outProtein} {outGFF}"
        results = os.system(myGFFReadCmd)
        if (results>0):
            print("Error: gffread failed\n")

#        cdsStr = f">{queryName}\n"
#        rr = open (outRNA, "rt")
#        next(rr)
#        for line in rr:
#            cdsStr += line
#        rr.close()
#
#        protStr = f">{queryName}\n"
#        rr = open (outProtein, "rt")
#        next(rr)
#        for line in rr:
#            cdsStr += line
#        rr.close()

        ## output information
        outStr = f"{queryName}\t{queryLen}\t{queryDesc}\t{largestGeneScaffold}\t{largestGeneStart}\t{largestGeneEnd}\t{largestGeneGenomicLen}\t{largestGeneExons}\t{largestGeneRNALen}\n"

        wh = open (outInfo, "w")
        wh.write(outStr)
        wh.close()

        os.remove(queryFile)
        os.remove(blastOutFile)
        os.remove(blastBEDFile)
        os.remove(mergedBED)
        os.remove(localGenomeFASTA)
        os.remove(localGenomeFASTACleaned)
        os.remove(exonerateGFF)



#     tblastn -num_threads 30 -out hh_blastOutput -db hh_genome -query human_kcnma1.fas -evalue 1e-20 -outfmt 6
#    ./manual_annotate_gene/blast2exonerate.py hh_blastOutput 1e-10
# sort -k1,1V -k2,2n hh_blastOutput.bed | bedtools merge -d 100000  | bedtools slop -i stdin -g hh_genome.txt -b 20000 > hh_new.bed
#bedtools getfasta  -fi  annotation_sphyrna_mokarran_06Feb2020_CUJmy.fasta -bed hh_new.bed > hh_new.fasta
# ./manual_annotate_gene/cleanseq.py hh_new.fasta hh_new2.fasta
#exonerate -q human_kcnma1.fas -Q protein -t hh_new2.fasta -T dna -m protein2genome -n 5 --minintron 20 --maxintron 100000  -f -100 --showtargetgff yes --showvulgar no --showalignment no  > hh_exonerate.gff
#/manual_annotate_gene/exonerate2gff3.py hh_exonerate.gff 
# gffread -g hh_new2.fasta  -w hh_cds.fas hh_exonerate.gff.gff3





    
if __name__== "__main__":
    main()
