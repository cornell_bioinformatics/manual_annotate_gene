#!/usr/bin/env python3
import sys
import re

inputgff = sys.argv[1]

gene_id=0

wh = open (f"{inputgff}.gff3", "w") 
with open (inputgff, "r") as rh:
    for line in rh:
        global gid
        line=line.strip().split("\t")
        if ((len(line)>5) and (line[2]=="gene")):
            scaffold=line[0]
            start = line[3]
            end = line[4]
            annot=line[8]
            match = re.search("sequence (\S+)", annot)
            query = match[1]
            gid = f"{query}.{gene_id}.{scaffold}.{start}.{end}"
            gene_id += 1
            line[8] = f"ID={gid};Name={gid}" 
            wh.write("\t".join(line))
            wh.write("\n")

            line[2]="mRNA"
            line[8] = f"ID={gid}-r;Parent={gid}" 
            wh.write("\t".join(line))
            wh.write("\n")
        if ((len(line)>5) and (line[2]=="cds")):
            line.append("")
            line[2] = "exon"
            line[8] = f"ID={gid}-e;Parent={gid}-r"
            wh.write("\t".join(line))
            wh.write("\n")

            line[8] = f"ID={gid}-cds;Parent={gid}-r"
            line[2] = "CDS"
            wh.write("\t".join(line))
            wh.write("\n")



rh.close()

       
